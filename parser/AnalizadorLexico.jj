PARSER_BEGIN(Analizador)
package  parser;

import errores.ManejadorErrores;
import simbolos.TablaDeSimbolos;
import simbolos.Variable;
import tiposDeDatos.TiposDeDatos;
import tiposDeDatos.Valor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;


class Analizador {

    public static TablaDeSimbolos tablaDeSimbolos;

        public static void main(String[] args) throws ParseException, FileNotFoundException, IOException, InterruptedException
        {
            tablaDeSimbolos = new TablaDeSimbolos();

            InputStream fis;

            if (System.in.available() > 0){
                fis = System.in;
            } else {
                fis = new FileInputStream("src/archivosDeEntrada/pruebaExito.txt");
            }

            try
            {
                Analizador analizador=new Analizador(fis);
                analizador.programa();
            }
            catch(ParseException e)
            {
              e.printStackTrace();
            }
            catch(Error e)
            {
              e.printStackTrace();
            }

//            ManejadorErrores.printSoloSemanticErrors();
            ManejadorErrores.print();

            Thread.sleep(100);

            System.out.println("\u005cnTabla de simbolos");
            tablaDeSimbolos.print();
        }

}

PARSER_END(Analizador)
/* Ignorados */
SKIP: {" " | "\t" | "\r" | "\n" }

/* Palabras Reservadas */
TOKEN :
{
< MODIFICADOR: "private" | "public" >
| < CLASE: "class" >
| < TIPO: "boolean" | "int"  | "double" | "string">
| < VOID: "void">
| < SI: "if" >
| < MIENTTRAS: "while" >
}

/* Separadores */
TOKEN :
{
  < PARENTESIS_IZQ: "(" >
| < PARENTESIS_DER: ")" >
| < CORCHETE_IZQ: "{" >
| < CORCHETE_DER: "}" >
| < PUNTO_Y_COMA: ";" >
| < COMA: "," >
| < PUNTO: ".">
}


/* Operadores */
TOKEN :
{
  < ASIGNACION: "=" >
| < COMPARADORES: ">" | "<" | "==" | "<=" | ">=" | "!=" >
| < MAS: "+" >
| < MENOS: "-" >
| < POR: "*">
| < ENTRE: "/">
}

/* Literales */
TOKEN:
{
< LITERAL_ENTERA: ["0"-"9"](["0"-"9"])* >
|< LITERAL_BOOLEANA: "false" | "true" >
|< LITERAL_CADENA: ("\"" (~["\""])* "\"") >
|< LITERAL_DOBLE: <LITERAL_ENTERA> <PUNTO> <LITERAL_ENTERA> >
}

/* simbolos.Identificador */
TOKEN:
{
	< IDENTIFICADOR:["a"-"z","A"-"Z","_"](["a"-"z","A"-"Z","_","-","0"-"9"])* >
}


void programa():{}{
	try{
		<MODIFICADOR> <CLASE> <IDENTIFICADOR> <CORCHETE_IZQ> (expresion())* <CORCHETE_DER>
	}catch(ParseException e){
		System.out.println(e.toString());
	}

}

// INICIA DECLARACION DE VARIABLES -

void declaracion_de_variable():{
    Token token = null;
    Token tokenTipoDatos = null;
    Token tokenAlcance = null;
    Variable variable = null;

}{
	tokenAlcance = <MODIFICADOR>
	tokenTipoDatos  = <TIPO>
	token           = <IDENTIFICADOR>
    {

        try {
            variable = new Variable(token,tokenTipoDatos,tokenAlcance);
            tablaDeSimbolos.agregarIdentificador(variable);
        } catch (Exception exception){
            ManejadorErrores.agregarException(exception);
        } catch (Error e){
            ManejadorErrores.agregarError(e);
        }
	}
	[asignacion(variable)]
}

//TERMINA DECLARACION DE VARIABLES

//INICIA ASIGNACION DE VARIABLES
void asignacion(Variable variable):{
    Valor valor;
}{
    <ASIGNACION>
    valor = expresion_matematica()
    {variable.setValor(valor);}
}

void expresion_asignacion():{
    Variable variable;
}{
    variable = variable()
	asignacion(variable)
}
//TERMINA ASIGNACION DE VARIABLES

Variable variable():{
    Token tokenIdentificador;
}{
    tokenIdentificador = <IDENTIFICADOR>
    {
      Variable variable = (Variable) tablaDeSimbolos.obtenerIdentificador(tokenIdentificador);
      variable.setAparicionMasReciente(tokenIdentificador);
      return variable;
    }
}

Valor valor():{
    Valor valor;
    Variable variable;
}{
    (
        valor = literal()
        |
        variable = variable()
        {valor = variable.getValor();}
    )
    {return valor;}
}

Valor literal():{
    Token literalToken;
    Valor valorDeLiteral;
}{
    (
        literalToken = <LITERAL_ENTERA>
        {valorDeLiteral = new Valor(literalToken.image,TiposDeDatos.getTipoDeDato("INT"));}
        |
        literalToken = <LITERAL_BOOLEANA>
        {valorDeLiteral = new Valor(literalToken.image,TiposDeDatos.getTipoDeDato("BOOLEAN"));}
        |
        literalToken = <LITERAL_CADENA>
        {valorDeLiteral = new Valor(literalToken.image,TiposDeDatos.getTipoDeDato("STRING"));}
        |
        literalToken = <LITERAL_DOBLE>
        {valorDeLiteral = new Valor(literalToken.image,TiposDeDatos.getTipoDeDato("DOUBLE"));}
    )

    {return valorDeLiteral;}
}


//INICIA OPERACIOONES MATEMATICAS

Valor expresion_matematica():{
    Valor resultado;
}{
    (
        LOOKAHEAD(2)
        resultado = operacion_matematica()
        |
        resultado = valor()
    )
    {return resultado;}
}

Valor operacion_matematica():{
    Valor operando1;
    Valor resultado = null;
}{
    operando1 = valor()
    resultado = operaciones(operando1)
    (resultado = operaciones(resultado))*

    {return resultado;}

}
Valor operaciones(Valor operando1):{
    Valor resultado;
}{
    (
        resultado = suma(operando1)
        |
        resultado = resta(operando1)
        |
        resultado = multiplicacion(operando1)
        |
        resultado = division(operando1)
    )
    {
        return resultado;
    }
}
Valor suma(Valor operando1):{
    Valor operando2;
    Token tokenOperador;
}{
    tokenOperador = <MAS>
    operando2 = valor()
    {return operando1.suma(operando2,tokenOperador);}
}

Valor resta(Valor operando1):{
    Valor operando2;
    Token tokenOperador;
}{
    tokenOperador = <MENOS>
    operando2 = valor()
    {return operando1.resta(operando2,tokenOperador);}
}

Valor division(Valor operando1):{
    Valor operando2;
    Token tokenOperador;
}{
    tokenOperador = <ENTRE>
    operando2 = valor()
    {return operando1.division(operando2,tokenOperador);}
}

Valor multiplicacion(Valor operando1):{
    Valor operando2;
    Token tokenOperador;
}{
    tokenOperador = <POR>
    operando2 = valor()
    {return operando1.multiplicacion(operando2,tokenOperador);}
}

//TERMINA OPERACIONES MATEMATICAS

void expresion():{}{
	(( expresion_asignacion() | declaracion_de_variable() ) <PUNTO_Y_COMA>) | expresiones_bloque()
}

void expresiones_bloque():{}{
    expresion_si() | expresion_mientras()
}

void expresion_si():{}{
	<SI> <PARENTESIS_IZQ> expresion_de_comparacion() <PARENTESIS_DER> bloque()
}

void expresion_mientras():{}{
	<MIENTTRAS> <PARENTESIS_IZQ> expresion_de_comparacion() <PARENTESIS_DER> bloque()
}

void bloque():{}{
    <CORCHETE_IZQ> (expresion())* <CORCHETE_DER>
}

void expresion_de_comparacion():{}{
	(<LITERAL_ENTERA> | <IDENTIFICADOR>) <COMPARADORES> (<LITERAL_ENTERA> | <IDENTIFICADOR>)
}
